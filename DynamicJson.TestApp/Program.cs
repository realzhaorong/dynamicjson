﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DynamicJson.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========BEGIN DYNAMIC METHOD TESTING========");
            DynamicMethod();
            Console.WriteLine("========BEGIN DYNAMIC JSON CONVERT TESTING========");
            DynamicJsonConvert();
            Console.WriteLine("========BEGIN OBJECT JSON CONVERT TESTING========");
            ObjectJsonConvert();
            Console.Read();
        }

        static void DynamicMethod()
        {
            dynamic dynamicObject = new DynamicClass();

            dynamicObject.Name = "damon";
            dynamicObject.Email = "zhaorong@outlook.com";

            dynamicObject.SendEmail = DelegateObject.Function((sender, param) =>
            {
                Console.WriteLine(string.Format("Name:{0}", sender.Name));
                Console.WriteLine(string.Format("Email:{0}", sender.Email));
                Console.WriteLine(string.Format("Content:{0}", param[0]));
                return null;
            });

            dynamicObject.SendEmail("Hello, Damon, This is e test email.");
        }

        static void DynamicJsonConvert()
        {
            string jsonString = "{\"error_code\":0,\"reason\":\"Success\",\"result\":{\"area\":\"Beijing\",\"location\":\"Telecom\"}}";

            Console.WriteLine(jsonString);

            var dyobj = JsonConverter.Deserialize(jsonString);

            Console.WriteLine(dyobj.error_code);
            Console.WriteLine(dyobj.reason);
            Dictionary<string, object> dictionary = dyobj.result as Dictionary<string, object>;
            Console.WriteLine(dictionary["area"]);
            Console.WriteLine(dictionary["location"]);
        }

        static void ObjectJsonConvert()
        {
            List<object> workexps = new List<object>();
            var _2010 = new { year_from = 2010, year_end = 2012, company = "Chuangrui, Anhui." };
            var _2012 = new { year_from = 2012, year_end = 2013, company = "Nero AG, Hangzhou." };
            var _2013 = new { year_from = 2013, year_end = 2015, company = "Boyin, Kunming." };
            var _2015 = new { year_from = 2015, year_end = "now", company = "Ruili Airline, Kunming." };

            workexps.Add(_2010);
            workexps.Add(_2012);
            workexps.Add(_2013);
            workexps.Add(_2015);

            var person = new { name = "damon", email = "zhaorong@outlook.com", workexps = workexps };

            string jsonString = JsonConvert.SerializeObject(person);

            Console.WriteLine(jsonString);

            var dyobj = JsonConverter.Deserialize(jsonString);

            Console.WriteLine(dyobj.name);
            Console.WriteLine(dyobj.email);
            var _workexps = dyobj.workexps as IList;
            foreach(var t in _workexps)
            {
                var dic = t as IDictionary;
                Console.WriteLine(string.Format("   {0}-{1}:{2}", dic["year_from"], dic["year_end"], dic["company"]));
            }
        }
    }
}
