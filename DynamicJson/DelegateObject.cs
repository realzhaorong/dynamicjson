﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicJson
{
    public delegate object DynamicDelegate(dynamic Sender, params object[] parameters);

    public class DelegateObject
    {
        private DynamicDelegate _delegate;

        public DynamicDelegate CallMethod
        {
            get { return _delegate; }
        }
        private DelegateObject(DynamicDelegate _dalegate)
        {
            this._delegate = _dalegate;
        }

        public static DelegateObject Function(DynamicDelegate _dalegate)
        {
            return new DelegateObject(_dalegate);
        }
    }
}
