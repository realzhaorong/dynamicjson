﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DynamicJson
{
    [DataContract]
    public class DynamicClass : DynamicObject
    {
        public IDictionary<string, object> Values { get; set; }

        public DynamicClass()
        {
            Values = new Dictionary<string, object>();
        }

        public DynamicClass(IDictionary<string, object> dictionary)
        {
            Values = dictionary;
        }

        public object GetPropertyValue(string propertyName)
        {
            if (Values.ContainsKey(propertyName) == true)
            {
                return Values[propertyName];
            }
            return null;
        }

        public void SetPropertyValue(string propertyName, object value)
        {
            if (Values.ContainsKey(propertyName) == true)
            {
                Values[propertyName] = value;
            }
            else
            {
                Values.Add(propertyName, value);
            }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = GetPropertyValue(binder.Name);
            return result == null ? false : true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            SetPropertyValue(binder.Name, value);
            return true;
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            var theDelegateObj = GetPropertyValue(binder.Name) as DelegateObject;
            if (theDelegateObj == null || theDelegateObj.CallMethod == null)
            {
                result = null;
                return false;
            }
            result = theDelegateObj.CallMethod(this, args);
            return true;
        }
        public override bool TryInvoke(InvokeBinder binder, object[] args, out object result)
        {
            return base.TryInvoke(binder, args, out result);
        }
    }
}
